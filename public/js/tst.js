class Errors {
    constructor(){
        this.errors = {};
    }
    get(field){
        if(this.errors[field]){
            return this.errors[field][0];
        }
    }
    record(errors){
        this.errors = errors;
    }
    clear(field){
        delete this.errors[field];
    }
    has(field){
        return this.errors.hasOwnProperty(field);
    }
    any(){
        return Object.keys(this.errors).length > 0;
    }
}
class Form{
    constructor(data){
        this.data = data;
        for(let field in data){
            this[field] = data[field];
        }
        this.errors = new Errors()
    }
}
new Vue({
    el: '#app',
    data: {
        form: new Form({
           name: '',
           disc: ''
        }),

    },
    methods:{
        submitting(){
            axios.post('projects',this.$data)
                .then(this.onSucc)
                .catch(error => this.form.errors.record(error.response.data))
        },
        onSucc(response){
            alert(response.data.message);
            this.name='';
            this.disc='';
        }
    },

    // mounted(){
    //     axios.get('skills').then(response => console.log(response))
    // }
});