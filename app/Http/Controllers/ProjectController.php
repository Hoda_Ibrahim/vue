<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function create(){
        $projects = Project::all();
        return view('welcome', compact('projects'));
    }
    public function store(Request $request){
        $this->validate(request(), [
           'name' => 'required',
           'disc' => 'required'
        ]);
        Project::create([
            'name' => $request->name,
            'disc' => $request->disc,
        ]);

        return ['message' => 'succ'];
    }

}
