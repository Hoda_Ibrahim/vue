<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
    </head>
    <body>

    @foreach( $projects as $project)
        {{ $project->name }} <hr>
    @endforeach
    <div id="app">
        <form method="POST" action="/projects" @submit.prevent="submitting()" @keydown="form.errors.clear($event)">
            <div>
                <input type="text" name="name" placeholder="name" v-model="form.name" >
            </div>
            <span v-if="form.errors.has('name')" v-text="form.errors.get('name')">@{{ form.errors }}</span>
            <br>
            <div>
                <input type="text" name="disc" placeholder="disc" v-model="form.disc">
            </div>
            <span v-if="form.errors.has('disc')" v-text="form.errors.get('disc')"></span>
            <br>
            <button type="submit" :disabled="form.errors.any()"> Add </button>
        </form>
    </div>
    <script src="js/axios.js"></script>
    <script src="js/vue.js"></script>
    <script src="js/tst.js"> </script>
    </body>
</html>
